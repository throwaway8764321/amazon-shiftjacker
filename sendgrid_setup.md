SendGrid setup
--------------

1. Sign up for an account

2. Create an API key (https://app.sendgrid.com/settings/api_keys)

3. Verify your email address (https://app.sendgrid.com/settings/sender_auth/senders)

4. Store your API key in the environment

    E.g. `export SENDGRID_API_KEY='YOUR_API_KEY' >> ~/.profile`

5. Update email notification settings in `~/.config/shiftjacker/config.json`
