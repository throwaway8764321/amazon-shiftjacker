#!/usr/bin/env python
"""Automated Amazon Moment shift signup."""

import errno
import logging
import os
import sys
import time
from simplejson.errors import JSONDecodeError
from textwrap import dedent

import pendulum
import requests
import simplejson
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver import Chrome, ChromeOptions
from xdg import XDG_CACHE_HOME, XDG_CONFIG_HOME

_SENDGRID_INSTALLED = True
try:
    from sendgrid import SendGridAPIClient
    from sendgrid.helpers.mail import Mail
except ImportError:
    _SENDGRID_INSTALLED = False

__program__ = "shiftjacker"
__description__ = "Automated Amazon Moment shift signup."

LOG = logging.getLogger(__program__)


class ShiftJacker:
    """Essentials for Amazon Moment shift signup."""

    def __init__(self, config):
        LOG.debug("config = %s", config)
        self.config = config

        self._session = requests.Session()

        # only the user agent is required (unsure whether keep-alive is helpful)
        self._headers = {
            "Connection": "keep-alive",
            "User-Agent": "Mozilla/5.0",
        }

        today = pendulum.today()

        # search for shifts beginning 2 days out (the day after tomorrow)
        start = today.add(days=2)
        start_date = str(start.date())

        # specify search interval (the max range is 30 days)
        end = start.add(days=30)
        end_date = str(end.date())

        LOG.info("Configured to search for shifts from %s to %s", start_date, end_date)

        self._refresh_params = (
            ("startDate", start_date),
            ("endDate", end_date),
            ("intervalType", "Overtime"),
        )

        # NOTE: Shifts officially drop between 18:15-18:30 (in practice it is typically
        #       at 18:16 or 18:17). If they miss the drop window, they reattempt to drop
        #       between 19:00-19:15. In the rare case that the first drop fails, you've
        #       got ample time to be ready for the second.

        # configure the time at which to start trying to get shifts
        self.launch_time = today.at(*self.config["launch_time"])

        # configure the time at which to give up trying to get shifts
        self.abort_time = today.at(*self.config["abort_time"])

        # used to indicate whether this is the first request (in order to store pre-drop shifts)
        self._first_run = True

        self.shiftdb = {}

        self.data = {
            "pre_drop_keys": set(),
            "pre_drop_shifts": [],
            "dropped_keys": set(),
            "dropped_shifts": [],
            "signed_up_keys": set(),
            "signed_up_shifts": [],
        }

    def _exit_message(self, msg=None):
        """Log the closing state of the program before exiting."""
        LOG.debug("shiftdb = %s", self.shiftdb)

        LOG.debug("pre_drop_keys = %s", self.data["pre_drop_keys"])
        self.data["pre_drop_shifts"] = [
            self.shiftdb.get(k) for k in self.data["pre_drop_keys"]
        ]
        LOG.debug("pre_drop_shifts = %s", self.data["pre_drop_shifts"])

        self.data["dropped_keys"] = self.shiftdb.keys() - self.data["pre_drop_keys"]
        LOG.debug("dropped_keys = %s", self.data["dropped_keys"])
        self.data["dropped_shifts"] = [
            self.shiftdb.get(k) for k in self.data["dropped_keys"]
        ]
        LOG.debug("dropped_shifts = %s", self.data["dropped_shifts"])

        LOG.debug("signed_up_keys = %s", self.data["signed_up_keys"])
        self.data["signed_up_shifts"] = [
            self.shiftdb.get(k) for k in self.data["signed_up_keys"]
        ]
        LOG.debug("signed_up_shifts = %s", self.data["signed_up_shifts"])

        if msg is not None:
            LOG.info(msg)

        log_shifts(
            "The following shifts were available pre-drop:",
            self.data["pre_drop_shifts"],
        )

        # note that "new" is displayed in bold and then italics
        log_shifts(
            "The following \x1b[1m\x1b[3mnew\x1b[0m shifts were dropped:",
            self.data["dropped_shifts"],
        )

        log_shifts(
            "The following shifts were signed up for:", self.data["signed_up_shifts"]
        )

        if (
            self.data["pre_drop_shifts"]
            or self.data["dropped_shifts"]
            or self.data["signed_up_shifts"]
        ):
            self._send_email()

        self._update_data_export()

    def _get_login_cookies(self):
        """Return cookies after completing the login process."""
        # seconds to wait for an individual element after page has finished loading
        timeout = 20

        options = ChromeOptions()

        options.add_argument("headless")

        with Chrome(options=options) as browser:
            # open login page
            LOG.info("Opening login page")
            browser.get("https://na.amazonmoment.com/")

            # wait for username input field to load then type username
            username_field = WebDriverWait(browser, timeout).until(
                EC.presence_of_element_located((By.ID, "usernameInputField"))
            )
            LOG.info("Typing username")
            username_field.send_keys(self.config["username"])

            # wait for submit button to load then click it to proceed to next page
            submit_button = WebDriverWait(browser, timeout).until(
                EC.presence_of_element_located((By.ID, "submitButton"))
            )
            LOG.info("Proceeding to password entry")
            submit_button.click()

            # wait for password input field to load then type password
            password_field = WebDriverWait(browser, timeout).until(
                EC.presence_of_element_located((By.ID, "passwordInputField"))
            )
            LOG.info("Typing password")
            password_field.send_keys(self.config["password"])

            # wait for submit button to load then click it to proceed to next page
            submit_button = WebDriverWait(browser, timeout).until(
                EC.presence_of_element_located((By.ID, "submitButton"))
            )
            LOG.info("Clicking login button")
            submit_button.click()

            # wait until login process is complete
            # Note: I just picked an arbitrary element on the final page to wait for, so
            #       this can probably be improved
            WebDriverWait(browser, timeout).until(
                EC.presence_of_element_located((By.ID, "containerNavbar"))
            )

            # get a copy of cookies before closing the browser
            LOG.info("Harvesting session cookies")
            cookies = browser.get_cookies()
            LOG.debug("cookies = %s", cookies)

            return cookies

    def _get_shifts(self):
        """Return list of currently available shifts."""

        retries = 10

        for i in range(retries):
            response = self._refresh()

            status = response.status_code
            content_type = response.headers.get("Content-Type")

            # ensure that we have a valid response
            if status == 200 and content_type == "application/json;charset=UTF-8":
                return response.json()

            if i < retries - 1:
                LOG.warning(
                    "Error retrieving list of currently available shifts. Retrying..."
                )

        LOG.error("Unable to retrieve list of currently available shifts")

        return []

    def _get_wanted_shifts(self, shifts):
        """
        Given a list of shifts, return a list of the shifts that we want,
        sorted by priority.
        """
        wanted = []

        for shift in shifts:
            for exclude in self.config["scheduling"]["excludes"]:
                if shift["workgroup"].endswith(exclude):
                    continue

            for priority, (start, end) in enumerate(
                self.config["scheduling"]["priority"]
            ):
                if shift["start"].endswith(start) and shift["end"].endswith(end):
                    shift["priority"] = priority
                    wanted.append(shift)
                    break

        prioritized = sorted(wanted, key=lambda x: x.get("priority"))

        return prioritized

    def _make_html(self):
        """Return HTML message for use in email."""

        def iter_html():
            """Generate strings of HTML."""
            contents = [
                (
                    "The following shifts were available pre-drop:",
                    self.data["pre_drop_shifts"],
                ),
                (
                    "The following <i>new</i> shifts were dropped:",
                    self.data["dropped_shifts"],
                ),
                (
                    "The following shifts were signed up for:",
                    self.data["signed_up_shifts"],
                ),
            ]

            yield dedent(
                """\
                <span style="font-family: Courier, monospace;">
                <span style="font-size: 14px;">
                """
            )

            for msg, shifts in contents:
                yield "<p>"
                yield from format_shifts(msg, shifts)
                yield "</p>"

            yield dedent(
                f"""\
                </span>

                <span style="font-size: 12px;">
                <p>
                <i>Sent courtesy of {__program__}</i>
                </p>
                </span>
                </span>
                """
            )

        return "\n".join(iter_html())

    def _refresh(self):
        """GET available shifts from server."""
        refresh_response = self._session.get(
            "https://na.amazonmoment.com/goa/wfm/adjustmentIntervals",
            headers=self._headers,
            params=self._refresh_params,
            allow_redirects=False,
        )

        return refresh_response

    def _send_email(self):
        """Send an email message."""
        email = self.config["email"]

        if not email["enabled"]:
            return

        if not _SENDGRID_INSTALLED:
            LOG.error(
                "SendGrid's Python library is not installed. Unable to send email notifications."
            )
            return

        message = Mail(
            from_email=email["from"],
            to_emails=email["to"],
            subject=f"Amazon Moment shift drop for {pendulum.today().date()}",
            html_content=self._make_html(),
        )

        try:
            sgclient = SendGridAPIClient(os.environ.get("SENDGRID_API_KEY"))
            sgclient.send(message)
        except Exception as exc:
            LOG.error(exc)

    def _signup(self, shift):
        """POST shift signup to server."""
        LOG.debug("shift = %s", shift)

        signup_data = {
            "startDate": shift["start"],
            "endDate": shift["end"],
            "intervalType": shift["intervalType"],
            "site": shift["site"],
            "workgroup": shift["workgroup"],
            "id": shift["adjustmentIntervalId"],
        }

        LOG.debug("signup_data = %s", signup_data)

        signup_response = self._session.post(
            "https://na.amazonmoment.com/goa/wfm/adjustmentIntervals/assignments/",
            headers=self._headers,
            json=signup_data,
        )

        if signup_response.status_code == 200:
            LOG.info(
                "Successfully signed up for: %s to %s",
                signup_data["startDate"],
                signup_data["endDate"],
            )
            self.data["signed_up_keys"].add(shift["adjustmentIntervalId"])
        else:
            LOG.info(
                "Failed to sign up for: %s to %s",
                signup_data["startDate"],
                signup_data["endDate"],
            )
            try:
                displayable_message = signup_response.json().get("displayableMessage")
                if displayable_message:
                    LOG.debug(
                        "%s: %s",
                        displayable_message["code"],
                        displayable_message["message"],
                    )
            except JSONDecodeError:
                pass

    def _signup_for_shifts(self, shifts):
        """Sign up for a list of shifts."""
        for shift in shifts:
            self._signup(shift)

    def _update_data_export(self):
        "Append shift data to backup file"
        # path to application's cache directory
        cache_dir = XDG_CACHE_HOME / __program__

        # ensure application's cache directory exists, creating it if necessary
        os.makedirs(cache_dir, exist_ok=True)

        db_path = cache_dir / "db.json"

        data = {}

        try:
            with open(db_path) as fob:
                data = simplejson.load(fob)
        except FileNotFoundError:
            LOG.debug("Creating on-disk export of shift data")

        data.update(self.shiftdb)

        with open(db_path, "w") as fob:
            LOG.debug("Writing to on-disk export of shift data")
            simplejson.dump(data, fob)

    def jack(self):
        """Check if there are any desired shifts available and sign up for them if so."""

        # get list of currently available shifts
        shifts = self._get_shifts()

        now = pendulum.now().isoformat(timespec="seconds")

        # store currently available shifts in a temporary dictionary
        recentdb = {}
        for shift in shifts:
            shift["last_seen"] = now
            recentdb[shift["adjustmentIntervalId"]] = shift

        # if this is the first time being run, store the keys of pre-drop shifts
        if self._first_run:
            self.data["pre_drop_keys"] = recentdb.keys()
            LOG.debug("pre_drop_keys = %s", self.data["pre_drop_keys"])
            self._first_run = False

        if not recentdb:
            return

        new_keys = recentdb.keys() - self.shiftdb.keys()

        if not new_keys:
            return

        new_shifts = [recentdb.get(k) for k in new_keys]
        LOG.info("Found new shifts")
        LOG.debug("new_shifts = %s", new_shifts)

        # check if we want any of the new shifts
        wanted_shifts = self._get_wanted_shifts(new_shifts)
        if wanted_shifts:
            LOG.info("We found shifts that we want!")
            LOG.debug("wanted_shifts = %s", wanted_shifts)
            self._signup_for_shifts(wanted_shifts)

        # update database of all seen shifts
        self.shiftdb.update(recentdb)

    def login(self):
        """
        Complete the login process.

        Sometimes login will fail for unknown reasons under unreproducible
        circumstances.

        As such, retry login before finally failing.
        """

        retries = 5
        for i in range(retries):
            try:
                LOG.info("Attempting to log in")
                cookies = self._get_login_cookies()
            except TimeoutException:
                if i < retries - 1:
                    LOG.warning("Failed to log in. Retrying...")
                continue
            break
        else:
            LOG.fatal("Giving up")
            sys.exit(1)

        LOG.info("Successfully logged in")

        # transfer cookies into a requests session
        for cookie in cookies:
            self._session.cookies.set(cookie["name"], cookie["value"])

    def scrape(self):
        """
        Scrape shifts.

        1) Sign up for any available shifts (that we want)
        2) Wait until shortly before the drop
        3) Start hammering the server(s)
        4) Sign up for newly dropped shifts before anyone else
        5) Profit
        """

        LOG.info("Querying server(s) for currently available shifts")

        self.jack()

        # if it's past the abort time, run in single-shot mode and exit
        if pendulum.now() >= self.abort_time:
            self._exit_message("It is passed the abort time, exiting now")
            sys.exit(0)

        wait_until(self.launch_time)

        LOG.info(
            "Querying server for new shifts until %s", self.abort_time.format("h:mmA")
        )

        while True:
            self.jack()

            # continue trying until the specified time
            if pendulum.now() >= self.abort_time:
                self._exit_message("Abort time has been reached")
                sys.exit(0)

            # wait a short period before retrying
            time.sleep(0.1)

        self._exit_message()


def convert_time_string(string):
    """Create a DateTime instance from a string."""
    return pendulum.from_format(string, "YYYY-MM-DDTHH:mm", "local")


def format_shifts(msg, shifts):
    """Generator used to nicely format a list of shifts for email."""
    if shifts:
        yield f"<b>{msg}</b><br>"
        for start, end in get_timepairs(shifts):
            datestring = get_pretty_date_range(start, end)
            yield f"{datestring}<br>"


def get_config():
    """Return application configuration."""
    skeleton = {
        "username": "themanwithnoname",
        "password": "topsecret",
        "launch_time": [18, 14],
        "abort_time": [18, 31],
        "scheduling": {
            "priority": [
                ["09:30", "20:00"],
                ["10:30", "15:00"],
                ["11:30", "16:15"],
                ["14:30", "19:15"],
                ["15:30", "19:30"],
                ["17:00", "21:15"],
            ],
            "excludes": ["_CLEANER"],
        },
        "email": {
            "enabled": False,
            "from": "email@address.com",
            "to": "email@address.com",
        },
    }

    # path to application's config directory
    config_dir = XDG_CONFIG_HOME / __program__

    # ensure application's config directory exists, creating it if necessary
    os.makedirs(config_dir, exist_ok=True)

    config_path = config_dir / "config.json"

    try:
        with open(config_path) as fob:
            return simplejson.load(fob)
    except IOError as exc:
        LOG.fatal(exc)
        if exc.errno == errno.ENOENT:
            with open(config_path, "w") as fob:
                simplejson.dump(skeleton, fob, indent="  ")
                LOG.info("Created default config file '%s'", config_path)
        sys.exit(1)


def get_pretty_date_range(start, end):
    """Return a nicely formatted date range as a string."""
    return f"[{start.date()}] {start.time().format('h:mmA')} - {end.time().format('h:mmA')}"


def get_timepairs(shifts):
    """Given a list of shifts, return sorted list of tuples with start and end times."""
    return sorted(
        [
            (convert_time_string(s["start"]), convert_time_string(s["end"]))
            for s in shifts
        ]
    )


def log_shifts(msg, shifts):
    """Log a nicely formatted list of shifts."""
    if shifts:
        LOG.info(msg)
        for start, end in get_timepairs(shifts):
            LOG.info(get_pretty_date_range(start, end))


def main():
    """Start application."""
    setup_logging()

    config = get_config()

    shiftjacker = ShiftJacker(config)
    shiftjacker.login()
    shiftjacker.scrape()


def setup_logging():
    """
    Configure logging.

    Write timestamped messages DEBUG and higher to log file.
    Write messages INFO and higher to console.
    """
    # path to application's log directory
    log_dir = XDG_CACHE_HOME / __program__ / "log"

    # ensure application's log directory exists, creating it if necessary
    os.makedirs(log_dir, exist_ok=True)

    log_filename = pendulum.now().format("YYYY-MM-DDTHH:mm:ss") + ".log"

    log_path = log_dir / log_filename

    # ensure logger is not reconfigured
    if not LOG.hasHandlers():

        LOG.setLevel(logging.DEBUG)

        basefmt = "%(levelname)s: %(message)s"

        # configure timestamped file log (log level DEBUG)
        file_handler = logging.FileHandler(log_path)
        file_formatter = logging.Formatter(
            "%(asctime)s.%(msecs)03d " + basefmt, "%Y-%m-%d %H:%M:%S"
        )
        file_handler.setFormatter(file_formatter)
        LOG.addHandler(file_handler)

        # configure console log (log level INFO)
        console_handler = logging.StreamHandler()
        console_handler.setLevel(logging.INFO)
        console_handler.setFormatter(logging.Formatter(basefmt))
        LOG.addHandler(console_handler)


def wait_until(end_time):
    """
    Wait until a specific time.

    Credit to https://github.com/jgillick/python-pause
    """
    LOG.info("Waiting until %s before continuing", end_time.format("h:mmA"))

    while True:
        diff = (end_time - pendulum.now()).total_seconds()

        if diff <= 0:
            # time is up!
            break

        # logarithmic sleep intervals to minimize unnecessary iterations
        time.sleep(diff / 2)


if __name__ == "__main__":
    main()
