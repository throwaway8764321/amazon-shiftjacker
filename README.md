# amazon-shiftjacker

Having a tough time getting shifts on Amazon Moment? Don't want to spend your evenings on your phone or computer endlessly hitting *Refresh* in hopes of getting shifts that you want?

Then don't.

Use **shiftjacker** to sign up for the shifts that you want. It watches the drop like a hawk and signs up for the shifts that you want before anyone else has a chance.

To be clear, this was only ever intended for personal use. It was created in a couple of hours one evening and designed accordingly. There are no unit tests and it's not even packaged. That being said, configured correctly, it will sign up for desired shifts within a fraction of a second of the drop, far faster than any human ever could.

Requirements
------------

I was really tempted to run this on Amazon Web Services but thought better. It was intended for use on a Linux distro. YMMV with WSL.

* Python 3 (with pip)
* chromedriver
* virtualenv (optional)
* systemd timer or other job scheduler (strongly recommended)

Recommended Installation
------------------------

    # clone the repo and enter its directory

    # create virtualenv
    virtualenv venv

    # install requirements from PyPI
    ./venv/bin/python -m pip install -U --upgrade-strategy eager -r requirements.txt

    # schedule the application to run ~5 minutes before the drop time (see included systemd service and timer files)

Configuration
-------------

The first time that the program is run, it will create a default config file (likely found at `~/.config/shiftjacker/config.json`) and then exit. Update the config file with your Amazon Moment username and password. Also update your list of desired shifts in order of preference. You can exclude shifts that are in certain workgroups, e.g. `_CLEANER`. You can also change the "launch time" and "abort time". When the program is run, it will first check if there are any desired shifts available and sign up for them if so. Afterwards, it will sleep until the "launch time" (which should be approximately 1 minute before the shifts are actually dropped) upon which it will start hammering the server until the drop. When it sees any shifts that it wants, it will sign up for them in order of preference. Sometimes the drop times of different workgroups are staggered, so it will continue checking for wanted shifts until the "abort time", upon which time it will exit and (optionally) send you an email notification.

Email notifications can be enabled from within the config file. They contain a list of shifts that were available pre-drop, new shifts that were dropped, and shifts that were successfully signed up for. It uses SendGrid's API to send emails but you could easily port it to use something else such as *yagmail* for Gmail. Be sure to create an account with SendGrid and then verify the email address that you will be sending messages from. Store your `SENDGRID_API_KEY` in the environment. Personally, I prefer to store it in `~/.profile`. You may also choose to store it in `~/.bashrc` or another shell init file.

Setting up a job scheduler is beyond the scope of this README but if you're using systemd I'd recommend copying `shiftjacker.service` and `shiftjacker.timer` into `~/.config/systemd/user/` and then enabling it via `systemctl --user enable shiftjacker.timer`. Be sure to update the path to your installation in `shiftjacker.service` beforehand. Depending on your local time, you may also need to update the time that the program is started in `shiftjacker.timer`.

If running the program via a scheduler (e.g. systemd timer), be sure it has access to the `SENDGRID_API_KEY` environment variable. If you aren't sure whether it does, then it probably doesn't. If using systemd in user-mode, you can store the API key in a *.conf* file accessible to *environment.d* (e.g. `~/.config/environment.d/sendgrid.conf`). Otherwise you can use something like `systemctl --user set-environment` or `systemctl --user import-environment` late in your `.bashrc`. You can see the `sendgrid_setup.md` file for more information.

Usage
-----

    ./venv/bin/python ./shiftjacker.py

If you don't have email notifications enabled, you can find out what shifts you signed up for by checking the logs. Logs should be stored in `~/.cache/shiftjacker/logs/`. You can also (obviously) check your schedule on Amazon Moment.

License
-------

Copyright (c) 2020 DGAF <throwaway8764321@gmail.com>

Licensed under the WTFUW (whatever the fuck you want) license.
